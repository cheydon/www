# Copyright (c) 2023, Caleb Heydon
# All rights reserved.

import subprocess
import ffmpeg
import shutil
import os
from flask import Flask
from flask import render_template

app = Flask(__name__, static_url_path="/")


@app.route("/", methods=["GET"])
def index():
    return render_template("index.html", theme="light-theme", copyright=True)


@app.route("/projects", methods=["GET"])
def posts():
    return render_template("projects.html", theme="light-theme", copyright=True)


@app.route("/p/making-a-simple-linux-distro", methods=["GET"])
def linux_distro():
    return render_template("making-a-simple-linux-distro.html", theme="light-theme", copyright=True, post_title="Making a Simple Linux Distro")


@app.route("/v/<string:video>", methods=["GET"])
def video(video):
    return render_template("video.html", theme="light-theme", project_title=video, video=video)


def encode_video(video_input, video_output, size):
    stream = ffmpeg.input(video_input)
    stream = ffmpeg.filter(stream, "scale", size[0], size[1])
    stream = ffmpeg.output(
        stream, video_output, **{"vcodec": "libx264", "an": None, "sn": None}
    )
    ffmpeg.run(stream, overwrite_output=True)

    return (
        "in=tmp_"
        + str(size[1])
        + "p.mp4,stream=video,output=h264_"
        + str(size[1])
        + "p.mp4,playlist_name=h264_"
        + str(size[1])
        + "p.m3u8,iframe_playlist_name=h264_"
        + str(size[1])
        + "p_iframe.m3u8 "
    )


shutil.rmtree("tmp", ignore_errors=True)
os.mkdir("tmp")

resolutions = [480, 720, 1080, 1440, 2160, 4320]

for subdir, dirs, files in os.walk("videos"):
    if subdir.find("/") == -1 and subdir.find("\\") == -1:
        continue
    if subdir.count("\\"):
        name = subdir.split("\\")[-1]
    else:
        name = subdir.split("/")[-1]
    input_file = subdir + "/input.mp4"
    directory = "app/static/v/" + name

    if os.path.isfile(subdir + "/.noupdate") and os.path.isdir(directory):
        continue

    shutil.rmtree(directory, ignore_errors=True)
    os.mkdir("tmp/" + name)

    probe = ffmpeg.probe(input_file)
    info_stream = next(
        (stream for stream in probe["streams"]
         if stream["codec_type"] == "video"), None
    )
    size = int(info_stream["width"]), int(info_stream["height"])
    fps = int(info_stream["r_frame_rate"].split("/")[0])

    audio_output = "tmp/" + name + "/tmp_audio.mp4"
    stream = ffmpeg.input(input_file)
    stream = ffmpeg.output(
        stream, audio_output, **{"vn": None, "acodec": "aac", "ar": "48000", "sn": None}
    )
    ffmpeg.run(stream, overwrite_output=True)

    command = "packager "
    command += "in=tmp_audio.mp4,stream=audio,output=audio.mp4,playlist_name=audio.m3u8,hls_group_id=audio,hls_name=ENGLISH "

    video_outputs = []

    for resolution in resolutions:
        if resolution > size[1]:
            continue

        width = size[0] * (resolution / size[1])
        if resolution == 480 and int(width) == 853:
            width = 854
        else:
            width = int(width)

        new_size = (width, resolution)
        video_outputs.append("tmp/" + name + "/tmp_" +
                             str(new_size[1]) + "p.mp4")
        command += encode_video(input_file, video_outputs[-1], new_size)

    command += "--hls_master_playlist_output h264_master.m3u8 --mpd_output h264.mpd"
    subprocess.call(command, cwd=os.path.realpath("tmp/" + name), shell=True)

    os.remove(audio_output)
    for x in video_outputs:
        os.remove(x)
    shutil.copytree("tmp/" + name, directory)
    shutil.rmtree("tmp/" + name)

shutil.rmtree("tmp")

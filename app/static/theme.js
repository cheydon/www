/*
Copyright (c) 2022, Caleb Heydon
All rights reserved.
*/

function updateTheme() {
    if (window.matchMedia("(prefers-color-scheme: dark)").matches) {
        document.body.classList.remove("light-theme");
        document.body.classList.add("dark-theme");
    }
}

updateTheme();

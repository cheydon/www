/*
Copyright (c) 2023, Caleb Heydon
All rights reserved.
*/

function videoPlayer(url, container, mainContent = false) {
    if (!url.endsWith("/"))
        url += "/";

    if (!url.includes("://"))
        if (url.startsWith("/"))
            url = window.location.protocol + "//" + window.location.host + url;
        else
            url = window.location.href + "/" + url;

    var urlObject = new URL(url);
    var baseUrl = urlObject.protocol + "//" + urlObject.host + "/";
    var video = document.createElement("video");

    function preloadImage(url) {
        var image = document.createElement("img");
        image.src = url;
        image.hidden = true;
        container.appendChild(image);
    }

    function handlePlayPause() {
        if (player.isPaused()) {
            player.play();
        } else {
            player.pause();
            if (controls.classList.contains("hidden"))
                controls.classList.remove("hidden");
        }
    }

    if (navigator.userAgent.includes("iPhone")) {
        url += "h264_master.m3u8";
        video.src = url;
        video.setAttribute("controls", "");
        container.appendChild(video);

        return null;
    }

    preloadImage(baseUrl + "play.svg");
    preloadImage(baseUrl + "pause.svg");
    preloadImage(baseUrl + "volume.svg");
    preloadImage(baseUrl + "volume-muted.svg");
    preloadImage(baseUrl + "fullscreen.svg");
    preloadImage(baseUrl + "pip.svg");
    preloadImage(baseUrl + "settings.svg");

    url += "h264.mpd";
    var player = dashjs.MediaPlayer().create();

    var controls = document.createElement("div");
    controls.classList.add("video-controls");
    var mouseState = "none";
    var mouseHoveringTime = 0;
    video.onmouseenter = () => {
        mouseState = "video";
        if (controls.classList.contains("hidden"))
            controls.classList.remove("hidden");
        if (video.style.cursor == "none")
            video.style.cursor = "default";
    };
    video.onmousemove = video.onmouseenter;
    controls.onmouseenter = () => {
        mouseState = "controls";
        if (controls.classList.contains("hidden"))
            controls.classList.remove("hidden");
    };
    var onmouseleave = () => {
        mouseState = "none";
        if (!controls.classList.contains("hidden") && !player.isPaused())
            controls.classList.add("hidden");
    };
    video.onmouseleave = onmouseleave;
    controls.onmouseleave = onmouseleave;

    var playButton = document.createElement("div");
    playButton.classList.add("video-controls-button");
    playButton.classList.add("video-controls-button-left");
    playButton.style.backgroundRepeat = "no-repeat";
    playButton.style.backgroundPosition = "center";
    playButton.style.backgroundSize = "20px";
    playButton.style.backgroundImage = "url(" + baseUrl + "play.svg)";
    playButton.onclick = handlePlayPause;
    video.onclick = () => {
        handlePlayPause();
        video.onmouseenter();
    }
    video.oncontextmenu = e => {
        e.preventDefault();
    };
    if (mainContent) {
        document.addEventListener("keydown", e => {
            if (e.key == ' ') {
                e.preventDefault();
                handlePlayPause();
            }
        });
        video.autoplay = true;
    }
    video.onplay = () => {
        playButton.style.backgroundImage = "url(" + baseUrl + "pause.svg)";
    };
    video.onpause = () => {
        playButton.style.backgroundImage = "url(" + baseUrl + "play.svg)";
    };
    controls.appendChild(playButton);

    var volumeButton = document.createElement("div");
    volumeButton.classList.add("video-controls-button");
    volumeButton.classList.add("video-controls-button-left");
    volumeButton.style.backgroundRepeat = "no-repeat";
    volumeButton.style.backgroundPosition = "center";
    volumeButton.style.backgroundSize = "20px";
    volumeButton.style.backgroundImage = "url(" + baseUrl + "volume.svg)";

    var volumeSlider = document.createElement("input");
    volumeSlider.classList.add("video-controls-slider");
    volumeSlider.type = "range";
    volumeSlider.value = 100;
    volumeSlider.title = "volume";

    function unmute() {
        player.setMute(false);
        volumeButton.style.backgroundSize = "20px";
        volumeButton.style.backgroundImage = "url(" + baseUrl + "volume.svg)";

        if (player.getVolume() == 0) {
            player.setVolume(1)
            volumeSlider.value = 100;
        }
    }

    function mute() {
        player.setMute(true);
        volumeButton.style.backgroundSize = "9px";
        volumeButton.style.backgroundImage = "url(" + baseUrl + "volume-muted.svg)";
    }

    volumeButton.onclick = () => {
        if (player.isMuted()) {
            unmute();
        } else {
            mute();
        }
    };
    controls.appendChild(volumeButton);

    volumeSlider.oninput = () => {
        var volume = volumeSlider.value / 100;
        player.setVolume(volume);
        if (volume == 0) {
            mute();
        } else {
            unmute();
        }
    };
    controls.appendChild(volumeSlider);

    var fullscreenButton = document.createElement("div");
    fullscreenButton.classList.add("video-controls-button");
    fullscreenButton.classList.add("video-controls-button-right");
    fullscreenButton.style.backgroundRepeat = "no-repeat";
    fullscreenButton.style.backgroundPosition = "center";
    fullscreenButton.style.backgroundSize = "20px";
    fullscreenButton.style.backgroundImage = "url(" + baseUrl + "fullscreen.svg)";
    fullscreenButton.onclick = () => {
        if (window.innerHeight == screen.height) {
            document.exitFullscreen();
        } else {
            container.requestFullscreen();
        }
    };
    controls.appendChild(fullscreenButton);

    if (document.pictureInPictureEnabled) {
        var pipButton = document.createElement("div");
        pipButton.classList.add("video-controls-button");
        pipButton.classList.add("video-controls-button-right");
        pipButton.style.backgroundRepeat = "no-repeat";
        pipButton.style.backgroundPosition = "center";
        pipButton.style.backgroundSize = "20px";
        pipButton.style.backgroundImage = "url(" + baseUrl + "pip.svg)";
        pipButton.onclick = () => {
            if (document.pictureInPictureElement)
                document.exitPictureInPicture();
            else if (document.pictureInPictureEnabled)
                video.requestPictureInPicture();
        };
        controls.appendChild(pipButton);
    }

    var timeLabel = document.createElement("p");
    timeLabel.classList.add("video-controls-label");
    timeLabel.innerText = "0:00 / -:--";
    controls.appendChild(timeLabel);

    var barContainer = document.createElement("div");
    barContainer.classList.add("video-bar-container");
    var seeking = false;
    var wasPaused = false;
    barContainer.onmousedown = e => {
        if (e.button == 0) {
            player.seek((e.layerX / barContainer.clientWidth) * player.duration());
            seeking = true;
            wasPaused = player.isPaused();
            player.pause();
        }
    };
    document.addEventListener("mouseup", e => {
        if (e.button == 0) {
            if (seeking) {
                seeking = false;
                if (!wasPaused)
                    player.play();
            }
        }
    });
    document.addEventListener("mousemove", e => {
        if (seeking) {
            var time = (e.x - barContainer.getBoundingClientRect().left) / barContainer.clientWidth;
            time = Math.max(0, time);
            time = Math.min(1, time);
            player.seek(time * player.duration());
        }
    });
    controls.appendChild(barContainer);

    var backgroundBar = document.createElement("div");
    backgroundBar.classList.add("video-controls-background-bar");
    barContainer.appendChild(backgroundBar);

    var bufferBar = document.createElement("div");
    bufferBar.classList.add("video-controls-buffer-bar");
    bufferBar.style.width = "0px";
    barContainer.appendChild(bufferBar);

    var progressBar = document.createElement("div");
    progressBar.classList.add("video-controls-progress-bar");
    progressBar.style.width = "0px";
    barContainer.appendChild(progressBar);

    container.appendChild(video);
    container.appendChild(controls);

    container.classList.add("video-container");

    var loadingStarted = false;
    var bufferedMin = 0;
    player.initialize(video, url, false);

    var lastVideoTime = 0;
    var lastTime = new Date().getTime();
    setInterval(() => {
        var currentTime = new Date().getTime();
        var dt = currentTime - lastTime;
        lastTime = currentTime;

        if (player.getBufferLength() > 0)
            loadingStarted = true;

        if (loadingStarted) {
            var minutes = Math.floor(player.time() / 60);
            var seconds = Math.round(player.time() % 60);
            var totalMinutes = Math.floor(player.duration() / 60);
            var totalSeconds = Math.round(player.duration() % 60);
            timeLabel.innerText = minutes + ((seconds < 10) ? ":0" : ":") + seconds + " / " + totalMinutes + ((totalSeconds < 10) ? ":0" : ":") + totalSeconds;

            var progress = player.time() / player.duration();
            progressBar.style.width = Math.round(progress * backgroundBar.clientWidth) + "px";

            var buffered = Math.min(1, (player.time() + player.getBufferLength()) / player.duration());
            if (buffered > bufferedMin || bufferedMin - buffered > 0.01)
                bufferedMin = buffered;
            else if (buffered < bufferedMin)
                buffered = bufferedMin;
            bufferBar.style.width = Math.round(buffered * backgroundBar.clientWidth) + "px";
        }

        if (mouseState == "video" && !player.isPaused())
            mouseHoveringTime += dt;
        else
            mouseHoveringTime = 0;

        if (mouseHoveringTime >= 2000) {
            video.onmouseleave();
            mouseHoveringTime = 0;
            video.style.cursor = "none";
        }

        if (mouseState == "none" && !controls.classList.contains("hidden") && !player.isPaused() && loadingStarted)
            controls.classList.add("hidden");
        else if (player.isPaused() && controls.classList.contains("hidden"))
            controls.classList.remove("hidden");

        var videoTime = player.time();
        if (videoTime.toFixed(1) == player.duration().toFixed(1) && videoTime == lastVideoTime && !player.isPaused()) {
            player.seek(0);
            player.pause();
        }
        lastVideoTime = videoTime;
    }, 10);

    return player;
}
